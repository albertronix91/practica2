import java.util.Scanner;
public class verde_dieciocho {

	public static void main(String[] args) {

		Scanner teclado=new Scanner(System.in);
		int valor=0;
    	String continuar="S";
		
		do {
			System.out.println("1. Sumar");
			System.out.println("2. Restar");
			System.out.println("3. Multiplicar");
			System.out.println("4. Dividir");
			
			valor=teclado.nextInt();

		    if(valor==1) {
		    	System.out.println("Introduzca dos números enteros para sumar");
		    	int num1=teclado.nextInt();
		    	int num2=teclado.nextInt();
		    	int suma=num1+num2;
		    	System.out.println("La suma de " + num1 + "+" + num2 + " es " + suma);
		    	
		    	System.out.println("¿Desea hacer otra operación? (S/N)");
		    	continuar=teclado.next();
		    }
		    
		    else if(valor==2) {
		    	System.out.println("Introduzca dos números enteros para restar");
		    	int num1=teclado.nextInt();
		    	int num2=teclado.nextInt();
		    	int resta=num1-num2;
		    	System.out.println("La resta de " + num1 + "-" + num2 + " es " + resta);
		    	
		    	System.out.println("¿Desea hacer otra operación? (S/N)");
		    	continuar=teclado.next();
		    }
		    
		    else if(valor==3) {
		    	System.out.println("Introduzca dos números enteros para multiplicar");
		    	int num1=teclado.nextInt();
		    	int num2=teclado.nextInt();
		    	int multiplicacion=num1*num2;
		    	System.out.println("La multiplicación de " + num1 + "*" + num2 + " es " + multiplicacion);
		    	
		    	System.out.println("¿Desea hacer otra operación? (S/N)");
		    	continuar=teclado.next();
		    }
		    
		    else if(valor==4) {
		    	System.out.println("Introduzca dos números para dividir");
		    	double num1=teclado.nextDouble();
		    	double num2=teclado.nextDouble();
		    	double division=num1/num2;
		    	System.out.println("La division de " + num1 + "/" + num2 + " es " + division);
		    	
		    	System.out.println("¿Desea hacer otra operación? (S/N)");
		    	continuar=teclado.next();
		    }
		    
		    else {
		    	System.out.println("Valor incorrecto");
		    }
		    
		    
		}while(continuar.equalsIgnoreCase("S"));
			
	    teclado.close();
		System.out.println("Programa finalizado");
	}

}