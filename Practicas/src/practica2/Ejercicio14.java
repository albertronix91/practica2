package practica2;

import java.util.Scanner;

public class Ejercicio14 {

	public static void main(String[] args) {

		Scanner entrada=new Scanner(System.in);
		double valor_introducido;
		double suma=0;
		int contador=-1;
		System.out.println("Introduzca valores enteros positivos. Cuando introduzca 0 el programa se detendrá");
		
		do {
			valor_introducido=entrada.nextInt();
			suma=suma+valor_introducido;
			contador++;
		}while(valor_introducido!=0);
		
		double media=suma/contador;
		System.out.println("La suma de los " + contador + " números introducidos es " + (int)suma + " y su media es " + media);
		entrada.close();
	}

}
