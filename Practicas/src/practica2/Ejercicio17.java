package practica2;
import java.util.Scanner;

public class Ejercicio17 {

	public static void main(String[] args) {

		Scanner teclado = new Scanner(System.in);
		System.out.print("Introduzca el lado del cuadrado: ");
        int lado = teclado.nextInt();
        teclado.close();
        
        for(int fila=1; fila<=lado; fila++) {
        	for(int columna=1; columna<=lado; columna++) {
        		/*
        		 * Recorre las filas y columnas
        		 * desde 1 hasta la longitud total del lado introducido
        		 */
        		if(fila==1 || fila==lado || columna==1 || columna==lado) {
        			System.out.print("*");
        		}else{
        			System.out.print(" ");
        		}
        		/*
        		 * Si coindide alguna de las condiciones establecidas en el if pondrá un "*"
        		 * En caso contrario pondrá un " "
        		 */
        	}
        	System.out.println(); //Baja una linea cada vez que se completa una fila
       	}
	}
}
