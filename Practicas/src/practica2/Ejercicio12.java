package practica2;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		Scanner entrada=new Scanner(System.in);
		
		int acumulado=0;
		
		for (int i=0; i<8; i++) {
			System.out.println("Introduce un número entero");
			int numero=entrada.nextInt();
			acumulado=acumulado+numero;
		}
		
		System.out.println("La suma de los 8 números introducidos es " +acumulado);
		entrada.close();
	}
}
