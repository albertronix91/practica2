package practica2;

import java.util.Scanner;

public class Ejercicio13 {

	public static void main(String[] args) {

		Scanner entrada=new Scanner(System.in);
		int mayor=0;
		String seguir="";
		
		do {
			System.out.println("Introduzca un número");
			int numero=entrada.nextInt();
			if (numero>mayor) {
				mayor=numero;
			}

			System.out.println("¿Desea continuar introduciendo números? (S/N)");
			seguir=entrada.next();
			
		}while(seguir.equalsIgnoreCase("S"));
		
		System.out.println("El mayor valor introducido es " + mayor);
		entrada.close();
	}

}
