/**
 * Saludos.java
 * Programa simple que muestra varios mensajes en la consola del sistema.
 * ajp - 2018.09.19
 */

/*
 * Los comentarios son un elemento de Java
 */

/* LÉXICO DEL PROGRAMA SALUDOS
 * import
 * public
 * class
 * static
 * void
 * main
 * String
 * args
 * Scanner
 * new
 * System
 * int
 */

/*
 * Una sentencia es una unidad minima de ejecución de un programa Java
 */

/* DATOS PRIMITIVOS EN JAVA
 * byte
 * short
 * int
 * long
 * float
 * double
 * boolean
 * char
 */

/*
 * Los tipos de datos primitivos representan un único dato simple
 */

/*
 * int numOrder
 * String nombre
 */

/* PALABRAS RESERVADAS
 * class: Define la implementación de un tipo particular de objeto.
 * import: Se usa al comienzo de un archivo fuente para especificar clases o paquetes completos de Java para consultarlos más adelante sin incluir sus nombres de paquete en la referencia
 * int: Se utiliza para declarar una variable que puede contener un entero.
 * new: Se utiliza para crear una instancia de una clase o un objeto de matriz.
 * public: Se usa en la declaración de una clase, método o campo.
 * static: Se utiliza para declarar un campo, método o clase interna como un campo de clase.
 * void: Se usa para declarar que un método no devuelve ningún valor.
 */

/*
 * Las variables Java son un espacio de memoria en el que guardamos un determinado valor o dato.
 */

/* OPERADORES DE JAVA
* Multiplicación
/ División
% Modulo
+ Adición
– Resta
++ Operador de incremento, utilizado para incrementar el valor en 1
-- Operador de decremento , usado para incrementar el valor en 1
!  Operador lógico “no”, utilizado para invertir un valor booleano.
== Igual a: devuelve verdadero si el valor del lado izquierdo es igual al lado derecho.
!= No igual a: devuelve verdadero si el valor del lado izquierdo no es igual al lado derecho.
< menos que: el resultado verdadero si el valor del lado izquierdo es inferior al del lado derecho.
<= menor o igual que: devuelve verdadero si el valor del lado izquierdo es menor o igual que el lado derecho.
> Mayor que: devuelve verdadero si el valor del lado izquierdo es mayor que el lado derecho.
>= Mayor que o igual a: regresa verdadero si el valor del lado izquierdo es mayor o igual que el lado derecho.
&& AND lógico
|| O lógico
 */

package practica2; //Muestra el paquete que contiene la clase
import java.util.Scanner; //Importa el paquete necesario para introducir datos por teclado

public class Saludos { 
	public static void main(String[] args) {
		/* teclado es un canal de entrada por teclado a través de un Scanner.
		   Necesita importar el paquete java.util
		 */
		Scanner teclado = new Scanner(System.in);

		int numOrden;			// Variable numérica.
		String nombre = "";		// Variable de texto. 

		// Configura y muestra mensajes de bienvenida.
		numOrden = 1 ;
		System.out.println("Hola,"); //Muestra texto en consola
		System.out.print("Soy un modesto "); //Muestra texto en consola
		System.out.print("programa de ordenador...\n"); //Muestra texto en consola
		System.out.println("Bienvenido al Curso.\t" + "Este es el saludo nº " + numOrden); //Muestra texto en consola
		numOrden++; //Aumenta la variable numOrder en 1 unidad
		System.out.println("Bienvenido a Java.\t" + "Este es el saludo nº " + numOrden); //Muestra texto en consola
		System.out.println("\nFin programa...");  //Muestra texto en consola
		teclado.close();
	}
}