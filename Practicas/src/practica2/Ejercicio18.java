package practica2;

import java.util.Scanner;

public class Ejercicio18 {

	public static void main(String[] args) {

		System.out.println("Introduzca un número");
		Scanner entrada=new Scanner(System.in);
		int numero=entrada.nextInt();
		int resultado=1;
		
		for (int i=numero; i>0; i--) {
			resultado=resultado*i;
		}
		
		System.out.println("El factorial de " + numero + " es " + resultado);
		entrada.close();
	}

}
