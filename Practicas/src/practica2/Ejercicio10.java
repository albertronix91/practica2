package practica2;
import java.util.Scanner; 

public class Ejercicio10 {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		
		System.out.println("Introduce un número entero");
		int a=entrada.nextInt();
		System.out.println("Introduce otro número entero");
		int b=entrada.nextInt();
		System.out.println("Introduce el último número entero");
		int c=entrada.nextInt();
		
		int menor=0;
		int intermedio=0;
		int mayor=0;
		
		if (a<b && a<c) {
			menor=a;
		}
		else if (b<a && b<c) {
			menor=b;
		}
		else if (c<a && c<b) {
			menor=c;
		}
		
		if (a>b && a>c) {
			mayor=a;
		}
		else if (b>a && b>c) {
			mayor=b;
		}
		else if (c>a && c>b) {
			mayor=c;
		}
		
		intermedio=a+b+c-mayor-menor;
		
		System.out.println("Los 3 valores ordenados son: " + menor + ", " + intermedio + ", " + mayor);
		entrada.close();
	}

}
