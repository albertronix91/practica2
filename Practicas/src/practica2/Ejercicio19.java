package practica2;
import java.util.Scanner;

public class Ejercicio19 {
	public static void main(String[] args) {
		
		System.out.println("Este programa resuelve ecuaciones de segundo grado");
		
		Scanner teclado = new Scanner(System.in);
		String repetir="";
		
		do {
			System.out.println("Introduce tres valores reales: a, b, y c");
			double a = teclado.nextDouble();
			double b = teclado.nextDouble();
			double c = teclado.nextDouble();
			double d = b*b-4*a*c;
			
			if(d<0) {
				System.out.println("No hay valores reales para esos datos");
				
				System.out.println("¿Intentar de nuevo? (S/N)");
				repetir=teclado.next();
			}
			
			else if (a==0 && b==0) {
				System.out.println("La ecuación es degenerada");
				
				System.out.println("¿Intentar de nuevo? (S/N)");
				repetir=teclado.next();
			}
			
			else if (a==0 && b!=0) {
				double x=-c/b;
				System.out.println("La ecuación tiene una única solución: x="+x);
				
				System.out.println("¿Intentar de nuevo? (S/N)");
				repetir=teclado.next();
			}
			
			else {
				double x1=(-b+Math.sqrt(d))/(2*a);
				double x2=(-b-Math.sqrt(d))/(2*a);
				
				System.out.println("Hay dos raices reales");
				System.out.println("x1=" + x1);
				System.out.println("x2=" + x2);
				
				System.out.println("¿Intentar de nuevo? (S/N)");
				repetir=teclado.next();
			}
		
		}while(repetir.equalsIgnoreCase("S"));
		
		teclado.close();
		System.out.println();
		System.out.println("Programa finalizado");
	}
}