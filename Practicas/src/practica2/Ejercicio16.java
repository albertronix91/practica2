package practica2;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		
		Scanner entrada=new Scanner(System.in);
		boolean finalizar;
		
		do {
			System.out.println("Introduzca un número con varios decimales, con coma");
			double numero=entrada.nextDouble();
			
			System.out.println("¿A cuantos decimales quiere redondear?");
			int decimales=entrada.nextInt();
			
			double multiplicador=Math.pow(10, decimales);
			double resultado=Math.rint(numero*multiplicador)/multiplicador;
			System.out.println(resultado);
			
			System.out.println("¿Desea finalizar el programa? (true/false)");
			finalizar=entrada.nextBoolean();
			
		}while(finalizar!=true);
		
		System.out.println("Programa finalizado");
		entrada.close();
	}

}
