package practica2;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {

		Scanner entrada=new Scanner(System.in);
		
		System.out.println("Introduce el capital inicial");
		double capital_inicial=entrada.nextInt();
		
		System.out.println("Introduce el tipo de interés anual en tanto por ciento");
		double interes=entrada.nextInt();
		
		System.out.println("Introduce el número de años");
		int años=entrada.nextInt();
		
		double capital_final=capital_inicial*Math.pow(1+interes/100, años);
		double intereses=capital_final-capital_inicial;
		
		System.out.print("El capital final es: ");
		System.out.printf("%1.2f", capital_final);
		System.out.print(" y los intereses son ");
		System.out.printf("%1.2f", intereses);
		
		entrada.close();
	}

}
